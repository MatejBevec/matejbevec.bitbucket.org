var map;
var polygons = [];
var clickRadius;
var radius = 500;

addEventListener("load", function(){

    // Pridobi podatke o bolnisnicah 
    $.get("https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", function(data){
        console.log(data);

        // Pridobi trenutno lokacijo uporabnika
        navigator.geolocation.getCurrentPosition(function(position){
            let {latitude:lat, longitude:lng} = position.coords;

            // Ustvari novo leaflet mapo
            map = L.map('map-div').setView([lat,lng], 15);
            map.on('click', onMapClick);
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);

            // Ustvari nov poligon za vsak objekt s pridobljenega seznama
            data.features.forEach(function(feature){
                if(feature.geometry.type == "Polygon"){
                    feature.geometry.coordinates.forEach(function(coordsInv){
                        // Dodaj poligone
                        let coords = coordsInv.map((item) => [item[1], item[0]]);
                        let poly = L.polygon(coords, {color: 'blue'}).addTo(map);
                        polygons.push(poly);
                        // Dodaj "popupe"
                        let p = feature.properties;
                        let popup = (p.name ? p.name + "<br>" : "")
                            + (p["addr:street"] ? p["addr:street"] : "")
                            + " " + (p["addr:housenumber"] ? p["addr:housenumber"] : "")
                            + (p["addr:city"] ? ", " +  p["addr:city"] : "") ;
                        if(!p.name && !p["addr:street"])
                            popup = "Ni informacij o objektu.";
                        
                        poly.bindPopup(popup);
                    });
                }

            });
        });
    });

});

// Ob kliku prestavi mapo na lokacijo klika in posodobi barve objektov v blizini
function onMapClick(e){
    if(map.hasLayer(clickRadius))
        map.removeLayer(clickRadius);

    clickRadius = L.circle(e.latlng, {color: 'red', opacity: 0.2, fillOpacity: 0.1, radius: radius})
        .addTo(map).bringToBack();
    map.setView(e.latlng);

    polygons.forEach(function(poly) {
        polyPos = poly._latlngs[0][0];
        if(distance(polyPos.lat, polyPos.lng, e.latlng.lat, e.latlng.lng, "K") * 1000 <= radius)
            poly.setStyle({color: 'green'});
        else
            poly.setStyle({color: 'blue'});
    });
}


