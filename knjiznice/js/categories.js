const bmiCategories = [
    {
        "category": "Severly underweight",
        "number": -2,
        "min": 0,
        "max": 16,
        "color": "#f45241"
    },
    {
        "category": "Underweight",
        "number": -1,
        "min": 16,
        "max": 18.5,
        "color": "#f4e241"
    },
    {
        "category": "Normal (healthy)",
        "number": 0,
        "min": 18.5,
        "max": 25,
        "color": "#5ef441"
    },
    {
        "category": "Overweight",
        "number": 1,
        "min": 25,
        "max": 30,
        "color": "#f4e241"
    },
    {
        "category": "Obese",
        "number": 2,
        "min": 30,
        "max": 100000,
        "color": "#f45241"
    }
];