
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
    return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(callback) {
    ehrId = "";

    // Generiranje naključnih smiselnih podatkov za pacienta

    var imena = ["Stane", "Ivan", "Lojze", "France", "Janko", "Metka", "Francka"];
    var priimki = ["Novak", "Horvat", "Mesar", "Ribic", "Peterle", "Jenko"];

    var datumRojstva = new Date(rnd(1910, 2010), rnd(1,12), rnd(1,28));
    var visina = rnd(150, 205);
    var ime = imena[rnd(0,imena.length-1)];
    var priimek = priimki[rnd(0,priimki.length-1)];

    var zacDatum = new Date(rnd(2000, 2018), rnd(1,12), rnd(1,28))
    var stMeritev = rnd(3,15);
    var zacTeza = rnd(50, 120);

    var baza = rnd(-4, 0, 4);
    var vari = rnd(0, 13);
    var meritve = [zacTeza];
    var datumi = [zacDatum];
    for(var i = 1; i < stMeritev; i++){
        var procent = baza + rnd(-vari, vari);
        meritve.push(meritve[i-1] + (procent / 100)*meritve[i-1]);
        let cas = datumi[i-1].getTime() + rnd(1,3) * 2592000000;
        datumi.push( new Date(cas) );
    }
    console.log(meritve, datumi);


    console.log(ime, priimek, datumRojstva, visina);

    // Vnesi podatke v EHRScape bazo
    $.ajaxSetup({
        headers: {
            "Authorization": getAuthorization()
        }
    });
    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',
        success: function (data) {
            var ehrId = data.ehrId;
            $("#header").html("EHR: " + ehrId);
    
            // build party data
            var partyData = {
                firstNames: ime,
                lastNames: priimek,
                dateOfBirth: datumRojstva,
                partyAdditionalInfo: [
                    {
                        key: "ehrId",
                        value: ehrId
                    }
                ]
            };
            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                success: function (party) {
                    if (party.action == 'CREATE') {
                        callback(ehrId);
                        datumi.forEach(function (item, i){
                            dodajMeritev(ehrId, meritve[i], datumi[i], visina);
                        });
                        
                    }
                }
            });
        }
    });

}

function generirajSeznam(){
    for(var i = 0; i < 3; i++){
        generirajPodatke(function(ehrId){
            console.log(ehrId);
            console.log($("suggested").text());
            $("#suggested").show();
            $("#suggested").append("<br>" + ehrId);
        });
    }
}

function dodajMeritev(ehrId, teza, cas, visina){
    $.ajaxSetup({
        headers: {
            "Authorization": getAuthorization()
        }
    });
    var compositionData = {
        "ctx/time": cas,
        "ctx/language": "en",
        "ctx/territory": "CA",
        "vital_signs/height_length/any_event/body_height_length": visina,
        "vital_signs/body_weight/any_event/body_weight": teza
    };
    var queryParams = {
        "ehrId": ehrId,
        templateId: 'Vital Signs',
        format: 'FLAT',
        committer: 'The Generator'
    };
    $.ajax({
        url: baseUrl + "/composition?" + $.param(queryParams),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(compositionData),
        success: function (res) {
            console.log("juhej");
            console.log(res);
        },
        error: function (e){
            console.log(e);
        }
    });
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

// Globalne spremenljivke
var patientId; // EhrId prijavljenega pacienta
// Pridobljeni podatki o prijavljenem pacientu
var patient = {
    ehrId:null,
    party:null,
    heights:null,
    weights:null,
};

addEventListener("load", function() {

    // Prijava pacienta z EhrID
    hideInfo();
    $("#suggested").hide();

    $("#login input[type=text]").on("keypress", function(e) {
        if (e.keyCode == 13) {
            login($(this).val());
        }
    });

    $("#login input[type=text]").focus(function(e) {
        $(this).val("");
        $("#suggested").show();
    });

});

function login(ehrId) {
    if (ehrId == "")
        ehrId = "5cdebdfa-0cd7-4d85-a1c1-dad97cee3da3";
    console.log("Logging in with " + ehrId);

    $.ajax({
        url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
        type: 'GET',
        headers: {
            "Authorization": getAuthorization()
        },
        error: function(xhr, status, error) {
            console.log(xhr.responseJSON);
            hideInfo();

            message(xhr.responseJSON.userMessage);
        },
        success: function(data) {
            console.log(data);

            // Lokalno prijavi pacienta
            patient.ehrId = ehrId;
            patient.party = data.party;

            // Pridobi potrebne podatke o prijavljenem pacientu
            renderBasicInfo();
            fetchData(ehrId);
            $("#suggested").hide();
        }

    });
}

function fetchData(ehrId) {

    $.ajax({
        url: baseUrl + "/view/" + ehrId + "/height",
        type: 'GET',
        headers: {
            "Authorization": getAuthorization()
        },
        success: function(heights) {
            console.log(heights);
            patient.heights = heights;

            $.ajax({
                url: baseUrl + "/view/" + ehrId + "/weight",
                type: 'GET',
                headers: {
                    "Authorization": getAuthorization()
                },
                success: function(weights) {
                    console.log("fetched weights");
                    console.log(weights);
                    patient.weights = weights;
                    renderBmiInfo(0);
                    renderChart();
                    renderWorldChart();
                    renderList();
                },
            });

        },
    });

}

function message(html) {
    $("#message").html(html);
}

// Rendiranje komponent

function renderBasicInfo(){
    console.log(patient.ehrId);
    $("#info").show();
    $("#basicInfo").show();
    $("#infoEhrId").text(patient.ehrId);
    $("#infoName").text(patient.party.firstNames + " " + patient.party.lastNames);
    $("#infoBirth").text(patient.party.dateOfBirth);
    $("#infoAge").text(age(patient.party.dateOfBirth));
    patient.party.gender ?  $("#infoGender").text(patient.party.gender) : "?";
}

function renderBmiInfo(index){
    console.log(patient.weights);
    $("#info").show();
    $("#bmiInfo").show();
    let lastHeight = patient.heights[0];
    let lastWeight = patient.weights[index];
    let bmi = calcBmi(lastWeight.weight, lastHeight.height/100, true); // Updatat za imperial enote
    let c = getBmiCategory(bmi);

    $("#infoTime").text(formatDate(lastWeight.time));
    $("#infoHeight").text(lastHeight.height + " " + lastHeight.unit);
    $("#infoWeight").text(lastWeight.weight + " " + lastWeight.unit);
    $("#infoBmi").text(bmi.toFixed(1));
    $("#infoCategory").text(c.category);
}

function hideInfo(){
    $("#info").hide();
    $("#basicInfo").hide();
    $("#bmiInfo").hide();
    $("#chart").hide();
    $("#list").hide();
    $("#worldChart").hide();
}

function renderChart(){
    console.log(calcBmiChart(patient));
    $("#chart").show();
    var ctx = document.getElementById('chartCanvas').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'scatter',

        // The data for our dataset
        data: {
            //labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [{
                label: 'Patient',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                showLine: true,
                data: calcBmiChart(patient),
                fill: false,
                //lineTension: 0,
            }]
        },

        // Configuration options go here
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        // Include a dollar sign in the ticks
                        callback: function(value, index, values) {
                            return (new Date(value)).toDateString().substring(3);
                        }
                    }
                }]
            }
        }
    });
}

function renderWorldChart(){
    $("#worldChart").show();
    var ctx = document.getElementById('worldChartCanvas').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'scatter',

        // The data for our dataset
        data: {
            //labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [{
                label: 'World average',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                showLine: true,
                data: getWorldAvg(),
                fill: false,
                //lineTension: 0,
            }]
        },

        // Configuration options go here
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        // Include a dollar sign in the ticks
                        callback: function(value, index, values) {
                            return value;
                        }
                    }
                }]
            }
        }
    });
}

function renderList(){
    $("#list").show();
    var lastHeight = patient.heights[0];
    patient.weights.forEach(function(item, index) {

        $("#list").append("<div class='listItem' index=" + index + "><span>" + formatDate(item.time)
             + " </span> <span class='label'>     " + calcBmi(item.weight, lastHeight.height/100, true).toFixed(1) + " BMI </span> </div>");

        
    })
    $("#list div").each(function() {
        $(this).click(function() {
            let index = parseInt($(this).attr("index"));
            renderBmiInfo(index);
            $("#list div").each(function(){$(this).removeClass("selected")});
            $(this).addClass("selected");

        });
    });
    
}
function hideList(){
    $("#list").hide();
}

// Izračuni

function age(dateofbirth) {
    var birthday = +new Date(dateofbirth);
    return ~~((Date.now() - birthday) / (31557600000));
}

function male(string){
    return (string.toLowerCase() == "male" || string.toLowerCase() == "man");
}

function calcBmi(mass, height, metric) {
    //console.log("calcBmi: " + mass + ", " + height);
    if(metric)
        return (mass / Math.pow(height, 2));
    else
        return (mass / Math.pow(height, 2)) * 703;
}

function calcBmiChart(patient){
    var lastHeight = patient.heights[0];

    let dataset = patient.weights.map((item) => {
        let x = new Date(item.time);
        let y = calcBmi(item.weight, lastHeight.height/100, true);
        return {x:x, y:y}
    });
    return dataset;
}

function getBmiCategory(bmi){
    console.log(bmi);
    let c = null;
    bmiCategories.forEach(function(category) {
        if(category.min <= bmi && bmi < category.max){
            c = category;
        }
    });
    return c;
}

function getWorldAvg(){
    var dataset = [];
    worldBmi.fact.forEach(function(item){
        if(item.dims.REGION == "(WHO) Global" && item.dims.SEX == "Both sexes"){
            let x = parseInt(item.dims.YEAR);
            let y = parseFloat(item.Value.substring(0,4));
            //console.log(x,y);
            dataset.push({x: x, y: y});
        }
    });
    console.log(dataset);
    return dataset;
}

function formatDate(date){
    return (new Date(date)).toDateString();
}

function rnd(min, max){
    return (min + Math.round(Math.random() * (max-min)));
}